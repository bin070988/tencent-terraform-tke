# tencent-terraform-tke

### Configuration
You will need to have a pair of secret id and secret key to access Tencent Cloud resources, configure it in the provider arguments or export it in environment variables. 
```
export TENCENTCLOUD_SECRET_ID=YOUR_SECRET_ID
export TENCENTCLOUD_SECRET_KEY=YOUR_SECRET_KEY
```

### Run
```
terraform init
terraform plan
terraform apply
```

If you want to destroy the resource, make sure the instance is already in running status, otherwise the destroy might fail.
```
terraform destroy
```